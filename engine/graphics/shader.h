#include <GL/glew.h>
#ifndef BARBARIACLION_SHADER_H
#define BARBARIACLION_SHADER_H

#include <iostream>
#include <string>
#include <fstream>

class Shader {
public:
    Shader(const char* vertexShaderPath, const char* fragmentShaderPath);
    void bind();
    std::string readShaderFile(const char *filePath);
    int getShaderLocation(const GLchar* variableName);
protected:
private:
    const char *vertexShaderSource;
    const char *fragmentShaderSource;
    int shaderProgram = 0;
};


extern Shader& returnMainShader();
extern Shader& returnLightingShader();

#endif //BARBARIACLION_SHADER_H
