#include "camera.h"

Camera::Camera() {
    cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
    cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
    cameraDirection = glm::normalize(cameraPos - cameraTarget);
    up = glm::vec3(0.0f, 1.0f, 0.0f);
    cameraRight = glm::normalize(glm::cross(up, cameraDirection));
    cameraUp = glm::cross(cameraDirection, cameraRight);
    viewMatrix = glm::lookAt(glm::vec3(0.0f, 0.0f, 3.0f),
                       glm::vec3(0.0f, 0.0f, 0.0f),
                       glm::vec3(0.0f, 1.0f, 0.0f));
    cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
}

void Camera::update(Shader& shader) {
    viewMatrix = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
    glUniformMatrix4fv(shader.getShaderLocation("cameraView"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
}

Camera& returnMainCamera(){
    static Camera mainCamera = Camera();
    return mainCamera;
}
