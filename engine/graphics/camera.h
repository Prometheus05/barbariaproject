#ifndef BARBARIACLION_CAMERA_H
#define BARBARIACLION_CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <definitions.h>

class Camera {
public:
    Camera();
    void update(Shader& shader);
    glm::vec3 cameraPos;
    glm::vec3 cameraFront;
    glm::vec3 cameraUp;
    glm::vec3 cameraTarget;
    glm::vec3 cameraDirection;
    glm::vec3 up;
    glm::vec3 cameraRight;
    glm::mat4 viewMatrix;
protected:
private:
};

extern Camera& returnMainCamera();

#endif //BARBARIACLION_CAMERA_H
