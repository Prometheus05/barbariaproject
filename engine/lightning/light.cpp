#include <iostream>
#include <graphics/shader.h>
#include "light.h"

Light::Light(){
    float vertices[] = {
            0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, //Bottom Left Front
            1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, //Bottom Right Front /
            1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, //Top Right Front
            0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, //Top Left Front
            0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, //Bottom Left Back
            1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, //Bottom Right Back
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, //Top Right Back
            0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f //Top Left Back
    };

    unsigned int indices[] = {
            0, 1, 2,
            0, 3, 2, //Front Face
            4, 5, 6,
            4, 7, 6, //Back Face
            3, 2, 6,
            3, 7, 6, //Top Face
            0, 1, 5,
            0, 4, 5, //Bottom Face
            0, 4, 7,
            0, 3, 7, //Left Face
            1, 5, 6,
            1, 2, 6 //Right Face
    };
    lightObject = new Object(vertices, 8, indices, 36);
    std::cout << "Buffer Code!" << std::endl;
    light = glm::vec3(1.0f, 1.0f, 1.0f);
}

void Light::update() {
    glUniform3fv(returnMainShader().getShaderLocation("lightColor"), 1, glm::value_ptr(light));
}

void Light::bind() {
    glUniform3fv(returnLightingShader().getShaderLocation("lightColor"), 1, glm::value_ptr(light));
    lightObject->update();
    lightObject->bind();
}

