#ifndef BARBARIACLION_LIGHT_H
#define BARBARIACLION_LIGHT_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <graphics/object.h>

class Light {
public:
    Light();
    void update();
    void bind();
    Object *lightObject;
protected:
private:
    glm::vec3 light;
};

#endif //BARBARIACLION_LIGHT_H
