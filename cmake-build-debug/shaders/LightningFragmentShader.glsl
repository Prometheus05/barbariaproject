#version 460 core

uniform sampler2D fragmentTexture;
uniform vec3 lightColor;

out vec4 fragmentColor;

void main() {
    //fragmentColor = texture(fragmentTexture, texCoord);
    vec3 objectColor = lightColor;
    fragmentColor = vec4(lightColor, 1.0);
}
