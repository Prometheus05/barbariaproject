#version 460 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

uniform mat4 modelMatrix;
uniform mat4 cameraView;
uniform mat4 projectionMatrix;

void main() {
    gl_Position = projectionMatrix * cameraView * modelMatrix * vec4(aPos.x, aPos.y, aPos.z, 1.0);
}
