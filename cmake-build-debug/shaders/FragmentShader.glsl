#version 460 core

uniform sampler2D fragmentTexture;
uniform vec3 lightColor;

out vec4 fragmentColor;

void main() {
    //fragmentColor = texture(fragmentTexture, texCoord);
    float ambientStrength = 0.1f;
    vec3 ambient = ambientStrength * lightColor;
    vec3 objectColor = vec3(1.0f, 0.5f, 0.31f);
    vec3 lightingResult = ambient * objectColor;
    fragmentColor = vec4(lightingResult, 1.0);
}
