#include "keyboard.h"

void processInput(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
    float cameraSpeed = 2.5f * deltaTime;
    if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        returnMainCamera().cameraPos += cameraSpeed * returnMainCamera().cameraFront;
    if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        returnMainCamera().cameraPos -= cameraSpeed * returnMainCamera().cameraFront;
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        returnMainCamera().cameraPos -= glm::normalize(glm::cross(returnMainCamera().cameraFront, returnMainCamera().cameraUp)) * cameraSpeed;
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        returnMainCamera().cameraPos += glm::normalize(glm::cross(returnMainCamera().cameraFront, returnMainCamera().cameraUp)) * cameraSpeed;
}