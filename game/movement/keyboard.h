#ifndef BARBARIACLION_KEYBOARD_H
#define BARBARIACLION_KEYBOARD_H

#include <graphics/camera.h>
#include <GLFW/glfw3.h>

extern void processInput(GLFWwindow *window);

#endif //BARBARIACLION_KEYBOARD_H
