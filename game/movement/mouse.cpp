#include "mouse.h"

float lastFrame = 0.0f;
float currentFrame = 0.0f;
float lastX = 960.0f;
float lastY = 540.0f;
float xoffset = 0.0f;
float yoffset = 0.0f;
float sensitivity = 0.05;
float yaw = -90.0f;
float pitch = 0.0f;
bool firstMouseMovement = true;
glm::vec3 direction;


void mouse_callback(GLFWwindow* window, double xpos, double ypos){
    if(firstMouseMovement){
        lastX = xpos;
        lastY = ypos;
        firstMouseMovement = false;
    }
    xoffset = xpos - lastX;
    yoffset = lastY - ypos;
    lastX = xpos;
    lastY = ypos;
    xoffset *= sensitivity;
    yoffset *= sensitivity;
    yaw += xoffset;
    pitch += yoffset;
    if (pitch > 89.0f)
        pitch = 89.0f;
    if (pitch < -89.0f)
        pitch = -89.0f;
    direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    direction.y = sin(glm::radians(pitch));
    direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    yaw = glm::mod( yaw + xoffset, 360.0f );
    returnMainCamera().cameraRight = glm::normalize(glm::cross(returnMainCamera().cameraFront, returnMainCamera().up));
    returnMainCamera().cameraUp = glm::normalize(glm::cross(returnMainCamera().cameraFront, returnMainCamera().up));
    returnMainCamera().cameraFront = glm::normalize(direction);
}