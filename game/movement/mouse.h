#ifndef BARBARIACLION_MOUSE_H
#define BARBARIACLION_MOUSE_H

#include <graphics/camera.h>
#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

extern float currentFrame;
extern float lastFrame;

extern void mouse_callback(GLFWwindow* window, double xpos, double ypos);

#endif //BARBARIACLION_MOUSE_H
