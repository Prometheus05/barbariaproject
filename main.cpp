#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <definitions.h>
#include <cmath>
#include "engine/graphics/shader.h"
#include "engine/graphics/object.h"
#include "game/movement/keyboard.h"
#include "game/movement/mouse.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <graphics/camera.h>
#include <lightning/light.h>

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

int main(){
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    GLFWwindow* gameWindow = glfwCreateWindow(1920, 1080, "Barbaria",  glfwGetPrimaryMonitor(), NULL);
    if(gameWindow == NULL) {
        std::cout << "Creation of gameWindow failed!" << std::endl;
        glfwTerminate();
        return -1;
    }
    glViewport(0, 0, 1920, 1080);
    glfwSetFramebufferSizeCallback(gameWindow, framebuffer_size_callback);
    glfwMakeContextCurrent(gameWindow);
    glewExperimental = GL_TRUE;
    glewInit();
    glfwSetInputMode(gameWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPosCallback(gameWindow, mouse_callback);

    glEnable(GL_DEPTH_TEST);

    /*float vertices[] = {
            0.5f,  0.5f, 0.0f, 0.0f, 0.0f, 0.0f, // top right
            0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 0.0f,  // bottom right
            -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 0.0f,   // bottom left
            -0.5f,  0.5f, 0.0f, 0.0f, 0.0f, 0.0f   // top left
    };

    unsigned int indices[] = {  // note that we start from 0!
            0, 1, 3,  // first Triangle
            1, 2, 3   // second Triangle
    }; */

    float vertices[] = {
            0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, //Bottom Left Front
            1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, //Bottom Right Front /
            1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, //Top Right Front
            0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, //Top Left Front
            0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, //Bottom Left Back
            1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, //Bottom Right Back
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, //Top Right Back
            0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f //Top Left Back
    };

    unsigned int indices[] = {
            0, 1, 2,
            0, 3, 2, //Front Face
            4, 5, 6,
            4, 7, 6, //Back Face
            3, 2, 6,
            3, 7, 6, //Top Face
            0, 1, 5,
            0, 4, 5, //Bottom Face
            0, 4, 7,
            0, 3, 7, //Left Face
            1, 5, 6,
            1, 2, 6 //Right Face
    };

    returnMainShader();

    glm::mat4 projectionMatrix = glm::perspective(glm::radians(45.0f), (float)1920/(float)1080, 0.1f, 100.0f);

    stbi_set_flip_vertically_on_load(true);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    int textureWidth, textureHeight, textureChannels;
    unsigned char* texture = stbi_load("texture.jpg", &textureWidth, &textureHeight, &textureChannels, 0);
    unsigned int textureBuffer;
    glGenTextures(1, &textureBuffer);
    glBindTexture(GL_TEXTURE_2D, textureBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, texture);
    glGenerateMipmap(GL_TEXTURE_2D);
    stbi_image_free(texture);

    Object myObject(vertices, 8, indices, 36);
    Light testLight = Light();
    testLight.lightObject->transform(0.0f, 0.0f, -6.0f);

    returnMainShader().bind();
    glUniformMatrix4fv(returnMainShader().getShaderLocation("projectionMatrix"), 1, GL_FALSE, glm::value_ptr(projectionMatrix));
    returnLightingShader().bind();
    glUniformMatrix4fv(returnLightingShader().getShaderLocation("projectionMatrix"), 1, GL_FALSE, glm::value_ptr(projectionMatrix));

    while(!glfwWindowShouldClose(gameWindow)){
        currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;
        glfwSwapBuffers(gameWindow);
        glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        returnMainShader().bind();
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureBuffer);
        returnMainCamera().update(returnMainShader());
        testLight.update();
        myObject.update();
        myObject.bind();
        returnLightingShader().bind();
        returnMainCamera().update(returnLightingShader());
        testLight.bind();
        processInput(gameWindow);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}